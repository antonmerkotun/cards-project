import Visit from "./Visit.js";

const visitTwo = new Visit();

export default function renderCard(card) {
    const cardZone = document.querySelector("#cardZone")

    cardZone.insertAdjacentHTML("afterbegin", `
        <div class="card" style="width: 18rem; margin: 10px auto">
            <div class="card-body">
                <div style="display: flex; justify-content: space-between">
                    <p class="card-text fullName">Имя: ${card.fullName}</p>
                    <button type="button" class="btn-close"></button>
                </div>
                <p class="card-subtitle mb-2 text-muted">${card.doctor}</p>
                <div class="cardWrapper" style="display: none">
                    <p class="card-text goal">${card.goal}</p>
                    <p class="card-text description">${card.description}</p>
                    <p class="card-text urgency">${card.urgency}</p>
                    <p class="card-text pressure">${card.pressure}</p>
                    <p class="card-text massIndex">${card.massIndex}</p>
                    <p class="card-text diseases">${card.diseases}</p>
                    <p class="card-text ageCardiologist">${card.ageCardiologist}</p>
                    <p class="card-text lastDate">${card.lastDate}</p>
                    <p class="card-text ageTherapist">${card.ageTherapist}</p>
                    <button id="btnVisit" type="button" class="btn btn-outline-warning edit" data-bs-toggle="modal" data-bs-target="#modalVisit">Редактировать</button>
                </div>
                <button type="button" class="btn btn-outline-info btnPlus">Показать больше</button>
            </div>
        </div>`);


    const cardBlock = document.querySelector(".card")
    cardBlock.classList.add(`${card.id}`)
    cardBlock.setAttribute("datatype", `${card.id}`)
    const btnPlus = document.querySelector(".btnPlus")
    const cardWrapper = document.querySelector('.cardWrapper')
    const cardText = document.querySelectorAll(".card-text")
    const btnClose = document.querySelector(".btn-close")


    btnClose.addEventListener("click", function () {
        fetch(`https://ajax.test-danit.com/api/v2/cards/${card.id}`, {
            method: 'DELETE',
            headers: {
                'Authorization': `Bearer ${localStorage.getItem("token")}`
            },
        }).then(response => response)
            .then(function (e) {
                fetch("https://ajax.test-danit.com/api/v2/cards", {
                    method: 'GET',
                    headers: {
                        'Authorization': `Bearer ${localStorage.getItem("token")}`
                    },
                })
                    .then(response => response.json())
                    .then(function (data) {
                        if (data.length <= 0){
                            const textCardZone = document.querySelector(".textCardZone")
                            if (data.length <= 0) {
                                textCardZone.textContent = "No items have been added"
                            } else {
                                textCardZone.textContent = ""
                            }
                        }
                    })
            })
        cardBlock.remove()

    })
    btnPlus.addEventListener("click", function () {
        cardWrapper.style.display = "block"
        btnPlus.style.display = "none"
        cardText.forEach(function (e) {
            if (e.textContent === "undefined") {
                e.textContent = ''
            }
        })
    })
    const btnEdit = document.querySelector(".edit")
    btnEdit.addEventListener("mouseup", function () {
        visitTwo.renderModalVisit(card);
        const btnVisitSave = document.querySelector("#btnVisitSave")
        btnVisitSave.style.display = "none"
        const btnVisitEdit = document.querySelector("#btnVisitEdit")
        btnVisitEdit.style.display = "block"
    })
};
