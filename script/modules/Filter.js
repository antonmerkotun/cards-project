"use strict"

const cardZon = document.querySelector('#cardZone')
const cards = cardZon.getElementsByClassName('card')
const controlForm = document.querySelector('.form-control')
const searchButton = document.querySelector('.btn-outline-secondary')

export default function search() {
    const inputValue = controlForm.value
    for (let i = 0; i < cards.length; i++) {
        const titles = cards[i].getElementsByClassName('card-text')
        const titleValue = titles[0].innerText
        const subtitles = cards[i].getElementsByClassName('card-subtitle')
        const subtitleValue = subtitles[0].innerText

        if (titleValue.indexOf(inputValue) !== -1 || subtitleValue.indexOf(inputValue) !== -1) {
            cards[i].style.display = 'block';
        } else {
            cards[i].style.display = 'none'
        }
    }
}
searchButton.addEventListener('click', search)

document.getElementById('selectPriority').addEventListener('change', function () {
    const selectedFilter = this.value
    if (selectedFilter !== 0) {
        for (let i = 0; i < cards.length; i++) {
            const priority = cards[i].getElementsByClassName('urgency')
            const priorityValue = priority[0].innerText
            if (priorityValue.indexOf(selectedFilter) !== -1) {
                cards[i].style.display = 'block';
            } else {
                cards[i].style.display = 'none'
            }
        }
    } else {
        for (let i = 0; i < cards.length; i++) {
            cards[i].style.display = 'block';
        }
    }
});
