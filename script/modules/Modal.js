'use strict'

import renderCard from "./Card.js";

export default class Modal {
    constructor(email, password) {
        this.email = email
        this.password = password
    }

    renderModalLogin() {
        const root = document.getElementById("root")
        root.insertAdjacentHTML("afterbegin", `
            <div class="modal fade" id="modalLogin" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
              <div class="modal-dialog">
                <div class="modal-content">
                  <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Вход</h5>
                  </div>
                  <div class="modal-body">
                    <input id="inputEmail" type="email" class="form-control" placeholder="Введите email" aria-label="Username" aria-describedby="addon-wrapping">
                    <input id="inputPassword" type="password" class="form-control mt-1" placeholder="Введите пароль" aria-label="Username" aria-describedby="addon-wrapping">
                  </div>
                  <div class="modal-footer">
                    <p id="error"></p>
                    <button id="btnModalSave" type="button" class="btn btn-primary">Вход</button>
                    <button id="btnModalClose" type="button" class="btn btn-outline-dark" data-bs-dismiss="modal">Закрыть</button>
                  </div>
                </div>
              </div>
            </div>
        `)
        this.setEmailLogin()
    }

    setEmailLogin() {
        const divVisit = document.querySelector("#modalLogin");
        const inputEmail = document.querySelector('#inputEmail');
        const inputPassword = document.querySelector('#inputPassword');
        const btnModalSave = document.querySelector('#btnModalSave');
        const btnModalClose = document.querySelector("#btnModalClose");
        const body = document.querySelector(".body")
        btnModalSave.addEventListener("click", () => {
            this.email = inputEmail.value
            this.password = inputPassword.value
            body.style.overflow = "visible"
            this.login()
        })
        btnModalClose.addEventListener('click', function () {
            divVisit.remove()
        })
    }

    login() {
        fetch("https://ajax.test-danit.com/api/v2/cards/login", {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({email: this.email, password: this.password})
        })
            .then(response => response.text())
            .then(function (data) {
                localStorage.setItem('token', data)
                if (data.length === 36) {
                    const divVisit = document.querySelector("#modalLogin")
                    const modalBack = document.querySelector(".modal-backdrop");
                    divVisit.remove()
                    modalBack.remove()
                    const btLog = document.querySelector("#btnLogin")
                    btLog.style.display = 'none'
                    const btVis = document.querySelector("#btnVisit")
                    btVis.style.display = 'block'
                    const btnExit = document.querySelector('#btnExit')
                    btnExit.style.display = 'block'

                    fetch("https://ajax.test-danit.com/api/v2/cards", {
                        method: 'GET',
                        headers: {
                            'Authorization': `Bearer ${localStorage.getItem("token")}`
                        },
                    })
                        .then(response => response.json())
                        .then(function (data) {
                            data.forEach(function (card) {
                                renderCard(card)
                            })
                            const textCardZone = document.querySelector(".textCardZone")
                            if (data.length <= 0) {
                                textCardZone.textContent = "No items have been added"
                            } else {
                                textCardZone.textContent = ""
                            }
                        })
                } else {
                    modal.errorMassage()
                }
            });
    }

    errorMassage() {
        const errorMassage = document.querySelector("#error")
        errorMassage.textContent = "Введен не правильно логин или пароль"
        errorMassage.style.color = "red"
        errorMassage.style.fontSize = "13px"
        errorMassage.style.marginRight = "30px"
    }
};

const modal = new Modal();
const buttonCreateLogin = document.querySelector("#btnLogin")
buttonCreateLogin.addEventListener('focus', function () {
    modal.renderModalLogin();
})