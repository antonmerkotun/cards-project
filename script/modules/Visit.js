'use strict'

import renderCard from "./Card.js";

export default class Visit {
    constructor(doctor, goal, description, urgency, fullName, id) {
        this.doctor = doctor
        this.goal = goal
        this.description = description
        this.urgency = urgency
        this.fullName = fullName
        this.id = id
    }

    renderModalVisit(card) {
        const root = document.getElementById("root")
        root.insertAdjacentHTML("afterbegin", `
            <div class="modal fade" id="modalVisit" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
              <div class="modal-dialog">
                <div class="modal-content">
                  <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Создать визит</h5>
                  </div>
                  <div id="modalContentVisit" class="modal-body">
                    <select id="doctor" class="form-select mt-1" aria-label="Default select example">
                        <option selected>Выберите врача</option>
                        <option style="doctorCardiologist" value="Кардиолог">Кардиолог</option>
                        <option style="doctorDentist" value="Стоматолог">Стоматолог</option>
                        <option style="doctorSelect" value="Терапевт">Терапевт</option>
                    </select>
                    <input id="goal" type="text" placeholder="Цель визита" class="form-control mt-1" aria-label="Sizing example input" aria-describedby="inputGroup-sizing-sm">
                    <input id="description" placeholder="Краткое описание" class="form-control mt-1" aria-label="Sizing example input" aria-describedby="inputGroup-sizing-sm">
                    <select id="urgency" class="form-select mt-1" aria-label="Default select example">
                        <option selected>Срочность</option>
                        <option value="Обычная">Обычная</option>
                        <option value="Приоритетная">Приоритетная</option>
                        <option value="Неотложная">Неотложная</option>
                    </select>
                    <input id="fullName" type="text" placeholder="ФИО" class="form-control mt-1" aria-label="Sizing example input" aria-describedby="inputGroup-sizing-sm">
                    <div id="inputDoctor"></div>
                  </div>
                  <div class="modal-footer">
                    <button id="btnVisitSave" type="button" class="btn btn-primary">Создать</button>
                    <button id="btnVisitEdit" type="button" class="btn btn-outline-success" style="display: none">Сохранить</button>
                    <button id="btnVisitClose" type="button" class="btn btn-outline-dark" data-bs-dismiss="modal">Закрыть</button>
                  </div>
                </div>
              </div>
            </div>
        `);
        const doctorInput = document.querySelector('#doctor')
        doctorInput.addEventListener("change", () => {
            if (card) {
                if (!card.pressure) {
                    card.pressure = ""
                }
                if (!card.massIndex) {
                    card.massIndex = ""
                }
                if (!card.diseases) {
                    card.diseases = ""
                }
                if (!card.ageCardiologist) {
                    card.ageCardiologist = ""
                }
                if (!card.lastDate) {
                    card.lastDate = ""
                }
                if (!card.ageTherapist) {
                    card.ageTherapist = ""
                }
            }
            visit.setDoctor(card)
        })

        if (card !== undefined) {
            document.querySelector("#goal").value = card.goal
            document.querySelector("#description").value = card.description
            document.querySelector("#urgency").value = card.urgency
            document.querySelector("#fullName").value = card.fullName
            this.setDoctorEdit(card)
        }
    }

    setDoctorEdit(card) {
        const doctorI = document.querySelector("#doctor")
        const inputDoctor = document.querySelector("#inputDoctor")
        if (card.doctor === "Кардиолог") {
            inputDoctor.remove()
            cardiologist.renderModalCardiologist(card)
            doctorI.value = "Кардиолог"
        }
        if (card.doctor === "Стоматолог") {
            inputDoctor.remove()
            dentist.renderModalDentist(card)
            doctorI.value = "Стоматолог"
        }
        if (card.doctor === "Терапевт") {
            inputDoctor.remove()
            therapist.renderModalTherapist(card)
            doctorI.value = "Терапевт"
        }
    }

    setDoctor(card) {
        const doctorI = document.querySelector("#doctor")
        const inputDoctor = document.querySelector("#inputDoctor")
        if (doctorI.value === "Кардиолог") {
            inputDoctor.remove()
            cardiologist.renderModalCardiologist(card)
        }
        if (doctorI.value === "Стоматолог") {
            inputDoctor.remove()
            dentist.renderModalDentist(card)
        }
        if (doctorI.value === "Терапевт") {
            inputDoctor.remove()
            therapist.renderModalTherapist(card)
        }
    }

    setObject() {
        this.doctor = document.querySelector("#doctor").value
        this.goal = document.querySelector("#goal").value
        this.description = document.querySelector("#description").value
        this.urgency = document.querySelector("#urgency").value
        this.fullName = document.querySelector("#fullName").value
    }

    fetchCard() {
        this.setObject()
        fetch("https://ajax.test-danit.com/api/v2/cards", {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
                'Authorization': `Bearer ${localStorage.getItem("token")}`
            },
            body: JSON.stringify({
                doctor: this.doctor,
                goal: this.goal,
                description: this.description,
                urgency: this.urgency,
                fullName: this.fullName,
                pressure: this.pressure,
                massIndex: this.massIndex,
                diseases: this.diseases,
                ageCardiologist: this.ageCardiologist,
                lastDate: this.lastDate,
                ageTherapist: this.ageTherapist,
            })
        })
            .then(response => response.json())
            .then((card) => {
                this.id = card.id
                renderCard(card)
                const textCardZone = document.querySelector(".textCardZone")
                textCardZone.textContent = ""
            });
        const modalVisit = document.querySelector("#modalVisit")
        const backModal = document.querySelector(".modal-backdrop.fade.show")
        modalVisit.remove()
        backModal.remove()
    }

    editCard(card) {
        this.setObject()
        fetch(`https://ajax.test-danit.com/api/v2/cards/${card.id}`, {
            method: 'PUT',
            headers: {
                'Content-Type': 'application/json',
                'Authorization': `Bearer ${localStorage.getItem("token")}`
            },
            body: JSON.stringify({
                doctor: this.doctor,
                goal: this.goal,
                description: this.description,
                urgency: this.urgency,
                fullName: this.fullName,
                pressure: this.pressure,
                massIndex: this.massIndex,
                diseases: this.diseases,
                ageCardiologist: this.ageCardiologist,
                lastDate: this.lastDate,
                ageTherapist: this.ageTherapist,
            })
        })
            .then(response => response.json())
            .then(function () {
                const cards = document.querySelectorAll(".card")
                cards.forEach(function (e) {
                    e.remove()
                })
                const divVisit = document.querySelector("#modalVisit")
                const modalBack = document.querySelector(".modal-backdrop");
                divVisit.remove()
                modalBack.remove()
                fetch("https://ajax.test-danit.com/api/v2/cards", {
                    method: 'GET',
                    headers: {
                        'Authorization': `Bearer ${localStorage.getItem("token")}`
                    },
                })
                    .then(response => response.json())
                    .then(function (data) {
                        data.sort((a, b) => a.id > b.id ? 1 : -1)
                        data.forEach(function (card) {
                            renderCard(card)
                        })
                    })
            })
    }
}
const visit = new Visit()
const modalVisit = document.querySelector("#btnVisit")
modalVisit.addEventListener('mouseup', function () {
    visit.renderModalVisit()
})

class VisitCardiologist extends Visit {
    constructor(doctor, goal, description, urgency, fullName, pressure, massIndex, diseases, ageCardiologist) {
        super(doctor, goal, description, urgency, fullName);
        this.pressure = pressure
        this.massIndex = massIndex
        this.diseases = diseases
        this.ageCardiologist = ageCardiologist
    }

    renderModalCardiologist(card) {
        const modalContentVisit = document.querySelector('#modalContentVisit');
        modalContentVisit.insertAdjacentHTML('beforeend', `
            <div id="inputDoctor" class="cardiologist">      
                <input id="pressure" type="text" placeholder="Обычное давление" class="form-control mt-1" aria-label="Sizing example input" aria-describedby="inputGroup-sizing-sm">
                <input id="massIndex" type="text" placeholder="Индекс массы тела" class="form-control mt-1" aria-label="Sizing example input" aria-describedby="inputGroup-sizing-sm">
                <input id="diseases" type="text" placeholder="Переносимые заболевания сердечно-сосудистой системы" class="form-control mt-1" aria-label="Sizing example input" aria-describedby="inputGroup-sizing-sm">
                <input id="ageCardiologist" type="text" placeholder="Возраст" class="form-control mt-1" aria-label="Sizing example input" aria-describedby="inputGroup-sizing-sm">
            </div>
        `)

        if (card !== undefined) {
            document.querySelector("#pressure").value = card.pressure
            document.querySelector("#massIndex").value = card.massIndex
            document.querySelector("#diseases").value = card.diseases
            document.querySelector("#ageCardiologist").value = card.ageCardiologist
        }

        const buttonSave = document.querySelector("#btnVisitSave")
        buttonSave.addEventListener("click", () => {
            const cardiologistDiv = document.querySelector(".cardiologist");
            if (cardiologistDiv) {
                this.setObjectCardio()
                this.fetchCard()
            }
        })

        const buttonEdit = document.querySelector("#btnVisitEdit")
        buttonEdit.addEventListener("click", () => {
            const cardiologistDiv = document.querySelector(".cardiologist");
            if (cardiologistDiv) {
                this.setObjectCardio()
                this.editCard(card)
            }
        })
    }

    setObjectCardio() {
        this.pressure = document.querySelector("#pressure").value
        this.massIndex = document.querySelector("#massIndex").value
        this.diseases = document.querySelector("#diseases").value
        this.ageCardiologist = document.querySelector("#ageCardiologist").value
    }

    fetchCard() {
        super.fetchCard();
    }

    editCard(card) {
        super.editCard(card);
    }
}

const cardiologist = new VisitCardiologist()

class VisitDentist extends Visit {
    constructor(doctor, goal, description, urgency, fullName, lastDate) {
        super(doctor, goal, description, urgency, fullName);
        this.lastDate = lastDate
    }

    renderModalDentist(card) {
        const modalContentVisit = document.querySelector('#modalContentVisit');
        modalContentVisit.insertAdjacentHTML('beforeend', `
            <div id="inputDoctor" class="dentist">      
                <input id="lastDate" type="text" placeholder="Дата последнего посещения" class="form-control mt-1" aria-label="Sizing example input" aria-describedby="inputGroup-sizing-sm"">
            </div>
        `)

        if (card !== undefined) {
            document.querySelector("#lastDate").value = card.lastDate
        }

        const buttonSave = document.querySelector("#btnVisitSave")
        buttonSave.addEventListener("click", () => {
            const dentistDiv = document.querySelector(".dentist")
            if (dentistDiv) {
                this.setObjectDentist()
                this.fetchCard()
            }
        })

        const buttonEdit = document.querySelector("#btnVisitEdit")
        buttonEdit.addEventListener("click", () => {
            const dentistDiv = document.querySelector(".dentist")
            if (dentistDiv) {
                this.setObjectDentist()
                this.editCard(card)
            }
        })
    }

    setObjectDentist() {
        this.lastDate = document.querySelector("#lastDate").value
    }

    fetchCard() {
        super.fetchCard();
    }

    editCard(card) {
        super.editCard(card);
    }
}

const dentist = new VisitDentist()

class VisitTherapist extends Visit {
    constructor(doctor, goal, description, urgency, fullName, ageTherapist) {
        super(doctor, goal, description, urgency, fullName);
        this.ageTherapist = ageTherapist
    }

    renderModalTherapist(card) {
        const modalContentVisit = document.querySelector('#modalContentVisit');
        modalContentVisit.insertAdjacentHTML('beforeend', `
            <div id="inputDoctor" class="therapist">      
                <input id="ageTherapist" type="text" placeholder="Возраст" class="form-control mt-1" aria-label="Sizing example input" aria-describedby="inputGroup-sizing-sm">
            </div>
        `)

        if (card !== undefined) {
            document.querySelector("#ageTherapist").value = card.ageTherapist
        }

        const buttonSave = document.querySelector("#btnVisitSave")
        buttonSave.addEventListener("click", () => {
            const therapistDiv = document.querySelector(".therapist")
            if (therapistDiv) {
                this.setObjectTherapist()
                this.fetchCard()
            }
        })

        const buttonEdit = document.querySelector("#btnVisitEdit")
        buttonEdit.addEventListener("click", () => {
            const therapistDiv = document.querySelector(".therapist")
            if (therapistDiv) {
                this.setObjectTherapist()
                this.editCard(card)
            }
        })
    }

    setObjectTherapist() {
        this.ageTherapist = document.querySelector("#ageTherapist").value
    }

    fetchCard() {
        super.fetchCard();
    }

    editCard(card) {
        super.editCard(card);
    }
}

const therapist = new VisitTherapist()

