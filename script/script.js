import Modal from "./modules/Modal.js";
import Visit from "./modules/Visit.js";
import renderCard from "./modules/Card.js"
import search from "./modules/Filter.js"


window.onload = function () {
    const cardZone = document.querySelector("#cardZone")
    const textCardZone = document.querySelector('.textCardZone')
    if (localStorage.getItem('token') !== null) {
        fetch("https://ajax.test-danit.com/api/v2/cards", {
            method: 'GET',
            headers: {
                'Authorization': `Bearer ${localStorage.getItem("token")}`
            },
        })
            .then(response => response.json())
            .then(function (data) {
                data.sort((a, b) => a.id > b.id ? 1 : -1)
                data.forEach(function (card) {
                    renderCard(card)
                })
                if (data.length <= 0) {
                    textCardZone.textContent = "No items have been added"
                } else {
                    textCardZone.textContent = ""
                }
            })

        const btnEnter = document.querySelector('#btnLogin')
        btnEnter.style.display = 'none'
        const btnExit = document.querySelector('#btnExit')
        btnExit.style.display = 'block'
        const btnVisit = document.querySelector("#btnVisit")
        btnVisit.style.display = 'block'
    }
};

const btnExit = document.querySelector('#btnExit')
btnExit.addEventListener('click', function () {
    localStorage.removeItem('token')
    location.reload()
});
